import "../styles/globals.scss";
import { DateContext, UserContext } from "@lib/context";
import { useState } from "react";
import Nav from "@components/Nav";
import { useUser } from "@utils/useUser";
import Login from "@components/Login";
import { Toaster } from "react-hot-toast";
import { setTime } from "@utils/date";

function MyApp({ Component, pageProps }) {
  const current = new Date();
  const [cursor, setCursor] = useState(new Date(setTime(current)));
  const [user, loading] = useUser();

  if (loading) return <div>Loading ...</div>;
  else {
    if (!user) return <Login />;
    else
      return (
        <DateContext.Provider value={{ cursor, setCursor }}>
          <UserContext.Provider value={{ user }}>
            <Toaster />
            <Nav />
            <Component {...pageProps} />
          </UserContext.Provider>
        </DateContext.Provider>
      );
  }
}

export default MyApp;
