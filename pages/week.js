import { useWeekCalendar } from "@lib/calendar";
import { DateContext, UserContext } from "@lib/context";
import { useContext, useState } from "react";
import CalendarHead from "@components/CalendarHead";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import Add from "@components/Add";
import { stringDateFromFirebase, stringDateFromDate } from "@utils/date";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";

import { firestore } from "@lib/firebase";
import toast from "react-hot-toast";

export default function Week() {
  const { cursor } = useContext(DateContext);
  const { user } = useContext(UserContext);
  const [currentWeek, loading] = useWeekCalendar(cursor, user);
  const [selectedDate, setSelectedDate] = useState(null);

  return (
    <div className="container calendar week">
      {loading ? (
        <div>Loading. .</div>
      ) : (
        <>
          {selectedDate && (
            <SelectDate
              document={selectedDate}
              setSelectedDate={setSelectedDate}
            />
          )}
          <CalendarHead />
          <RenderDay array={currentWeek} setSelectedDate={setSelectedDate} />
        </>
      )}
    </div>
  );
}

function RenderDay({ array, setSelectedDate }) {
  return array.map((doc) => (
    <div key={doc.date.toJSON()}>
      <h3 onClick={() => setSelectedDate(doc)}>
        {doc.date.toString().split(" ")[2]}
      </h3>
      <ul>
        {doc.appointment.map((app) => (
          <li key={app.id}>
            <b>{app.title}</b>
            <span>{app.detail}</span>
          </li>
        ))}
      </ul>
    </div>
  ));
}

function SelectDate({ document, setSelectedDate }) {
  const [addMenu, setAddMenu] = useState(false);
  const deleteHandle = (id) => {
    firestore.collection("dates").doc(id).delete();
    toast.success("burn appointment success 🔥🔥");
  };
  return (
    <div className="calendar__fullappointment">
      {addMenu ? <Add setAddMenu={setAddMenu} /> : null}
      <div className="calendar__fullappointment__container">
        <div className="calendar__fullappointment__container__header">
          <h1>{stringDateFromDate(document.date)}</h1>
          <div>
            <button onClick={() => setAddMenu(true)}>
              <AddIcon />
            </button>
            <button onClick={() => setSelectedDate(null)}>
              <CloseIcon />
            </button>
          </div>
        </div>
        <ul>
          {document.appointment.map((app) => (
            <li key={app.id}>
              <div className="calendar__fullappointment__container__inside">
                <h2>{app.title}</h2>
                <span>{app.detail}</span>
                <br />
                <span>
                  {stringDateFromFirebase(app.date)} to{" "}
                  {stringDateFromFirebase(app.endDate)}
                </span>
              </div>
              <div className="control">
                <button>
                  <EditIcon />
                </button>
                <button onClick={() => deleteHandle(app.id)}>
                  <DeleteIcon />
                </button>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
