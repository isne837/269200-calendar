import { useMonthCalendar } from "@lib/calendar";
import { DateContext, UserContext } from "@lib/context";
import { useState, useContext } from "react";
import CalendarHead from "@components/CalendarHead";
import { useClickOutSide } from "@lib/useClickOutSide";
import { stringDateFromFirebase, stringDateFromDate } from "@utils/date";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import Add from "@components/Add";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";

import { firestore } from "@lib/firebase";
import toast from "react-hot-toast";

export default function Home() {
  const { cursor } = useContext(DateContext);
  const { user } = useContext(UserContext);
  const [prevMonth, currentMonth, nextMonth, loading] = useMonthCalendar(
    cursor,
    user
  );

  const [selectedDate, setSelectedDate] = useState(null);
  return (
    <div className="container calendar">
      {loading ? (
        <div>laoding</div>
      ) : (
        <>
          {selectedDate && (
            <SelectDate
              document={selectedDate}
              setSelectedDate={setSelectedDate}
            />
          )}
          <CalendarHead />
          <RenderDay array={prevMonth} setSelectedDate={setSelectedDate} />
          <RenderDay array={currentMonth} setSelectedDate={setSelectedDate} />
          <RenderDay array={nextMonth} setSelectedDate={setSelectedDate} />
        </>
      )}
    </div>
  );
}

function RenderDay({ array, setSelectedDate }) {
  return array.map((doc) => (
    <div key={doc.date.toJSON()}>
      <h3 onClick={() => setSelectedDate(doc)}>
        {doc.date.toString().split(" ")[2]}
      </h3>
      <ul>
        {doc.appointment.map((app) => (
          <li key={app.id}>
            <b>{app.title}</b>
            <span>{app.detail}</span>
          </li>
        ))}
      </ul>
    </div>
  ));
}

function SelectDate({ document, setSelectedDate }) {
  const [addMenu, setAddMenu] = useState(false);
  const [editMenu, setEditMenu] = useState(null);
  const deleteHandle = (id) => {
    firestore.collection("dates").doc(id).delete();
    toast.success("burn appointment success 🔥🔥");
  };
  return (
    <div className="calendar__fullappointment">
      {addMenu ? <Add setAddMenu={setAddMenu} /> : null}
      {editMenu ? (
        <EditAppointment setEditMenu={setEditMenu} app={editMenu} />
      ) : null}
      <div className="calendar__fullappointment__container">
        <div className="calendar__fullappointment__container__header">
          <h1>{stringDateFromDate(document.date)}</h1>
          <div>
            <button onClick={() => setAddMenu(true)}>
              <AddIcon />
            </button>
            <button onClick={() => setSelectedDate(null)}>
              <CloseIcon />
            </button>
          </div>
        </div>
        <ul>
          {document.appointment.map((app) => (
            <li key={app.id}>
              <div className="calendar__fullappointment__container__inside">
                <h2>{app.title}</h2>
                <span>{app.detail}</span>
                <br />
                <span>
                  {stringDateFromFirebase(app.date)} to{" "}
                  {stringDateFromFirebase(app.endDate)}
                </span>
              </div>
              <div className="control">
                <button>
                  <EditIcon />
                </button>
                <button onClick={() => deleteHandle(app.id)}>
                  <DeleteIcon />
                </button>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

function EditAppointment({ app }) {
  const [titile, setTitle] = useState(app.title);
  const [detail, setDetail] = useState(app.detail);
  const onSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <div className="edit__appointment">
      <form onSubmit={onSubmit}>
        <input type="text" />
      </form>
    </div>
  );
}
