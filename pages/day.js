import { useDayCalendar } from "@lib/calendar";
import { DateContext, UserContext } from "@lib/context";
import { useContext, useState } from "react";
import { stringDateFromFirebase, stringDateFromDate } from "@utils/date";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import AddIcon from "@mui/icons-material/Add";
import Add from "@components/Add";

export default function Day() {
  const { cursor } = useContext(DateContext);
  const { user } = useContext(UserContext);
  const [currentDate, loading] = useDayCalendar(cursor, user);
  const [addMenu, setAddMenu] = useState(false);

  return (
    <div className="container">
      {loading ? (
        <div>Loading...</div>
      ) : (
        <>
          <div className="calendar__date">
            {addMenu ? <Add setAddMenu={setAddMenu} /> : null}
            <div className="calendar__date__header">
              <h1>{stringDateFromDate(cursor)}</h1>
              <button onClick={() => setAddMenu(true)}>
                <AddIcon />
              </button>
            </div>
            <ul>
              {currentDate.appointment.map((app) => (
                <li key={app.id}>
                  <div className="">
                    <h2>{app.title}</h2>
                    <span>{app.detail}</span>
                    <br />
                    <span>
                      {stringDateFromFirebase(app.date)} to{" "}
                      {stringDateFromFirebase(app.endDate)}
                    </span>
                  </div>
                  <div className="control">
                    <button>
                      <EditIcon />
                    </button>
                    <button>
                      <DeleteIcon />
                    </button>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </>
      )}
    </div>
  );
}
