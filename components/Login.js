import { auth, googleProvider } from "@lib/firebase";

export default function Login() {
  return (
    <div>
      LOGIN
      <SignInButton />
    </div>
  );
}

const SignInButton = () => {
  const onClick = async () => {
    await auth.signInWithRedirect(googleProvider);
  };
  return <button onClick={onClick}>LOGIN</button>;
};
