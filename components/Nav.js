import { useContext, useState } from "react";
import { DateContext, UserContext } from "@lib/context";
import { moveCursorDate, moveCursorMonth, month } from "@utils/date";
import { useRouter } from "next/router";
import Link from "next/link";
import { auth } from "@lib/firebase";
import Add from "@components/Add";
import Image from "next/image";
import SearchIcon from "@mui/icons-material/Search";
import EventIcon from "@mui/icons-material/Event";
import MenuIcon from "@mui/icons-material/Menu";
import CalendarViewWeekIcon from "@mui/icons-material/CalendarViewWeek";
import ViewDayOutlinedIcon from "@mui/icons-material/ViewDayOutlined";
import CalendarViewMonthOutlinedIcon from "@mui/icons-material/CalendarViewMonthOutlined";
import { useClickOutSide } from "@lib/useClickOutSide";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";

export default function Nav() {
  const { cursor, setCursor } = useContext(DateContext);
  const { user } = useContext(UserContext);
  const router = useRouter();
  const path = router.pathname;
  const [show, setShow] = useState(false);
  const popupRef = useClickOutSide(() => setShow(false));

  const handleRouter = (ref) => {
    router.push(ref);
    setShow(false);
  };

  return (
    <nav className="nav">
      <div
        ref={popupRef}
        className={show ? "nav__popup nav__show" : "nav__popup"}
      >
        <div className="nav__logo" onClick={() => handleRouter("/")}>
          <div className="logo">
            <Image src="/logo.png" width="40" height="40" alt="logo" />
          </div>
          Firebase 🔥
        </div>
        <ul>
          <li
            className={path === "/day" ? "current" : null}
            onClick={() => handleRouter("/day")}
          >
            <ViewDayOutlinedIcon />
            day
          </li>
          <li
            className={path === "/week" ? "current" : null}
            onClick={() => handleRouter("/week")}
          >
            <CalendarViewWeekIcon /> Week
          </li>
          <li
            className={path === "/" ? "current" : null}
            onClick={() => handleRouter("/")}
          >
            <CalendarViewMonthOutlinedIcon /> Month
          </li>
        </ul>
        <div className="auth">
          <div className="cotinaer">
            <div>
              <Image src={user.photoURL} width="50" height="50" alt="profile" />
            </div>
            <h3>{user.displayName}</h3>
          </div>
          <button onClick={() => auth.signOut()}>Logout</button>
        </div>
      </div>
      <div className="control">
        <button onClick={() => setShow(true)}>
          <MenuIcon />
        </button>
        {path === "/" ? (
          <button onClick={() => setCursor((c) => moveCursorMonth(c, -1))}>
            <ChevronLeftIcon />
          </button>
        ) : null}
        {path === "/day" ? (
          <button onClick={() => setCursor((c) => moveCursorDate(c, -1))}>
            <ChevronLeftIcon />
          </button>
        ) : null}
        {path === "/week" ? (
          <button onClick={() => setCursor((c) => moveCursorDate(c, -7))}>
            <ChevronLeftIcon />
          </button>
        ) : null}
        <h2>{monthString(cursor)}</h2>
        {path === "/" ? (
          <button onClick={() => setCursor((c) => moveCursorMonth(c, 1))}>
            <ChevronRightIcon />
          </button>
        ) : null}
        {path === "/day" ? (
          <button onClick={() => setCursor((c) => moveCursorDate(c, 1))}>
            <ChevronRightIcon />
          </button>
        ) : null}
        {path === "/week" ? (
          <button onClick={() => setCursor((c) => moveCursorDate(c, 7))}>
            <ChevronRightIcon />
          </button>
        ) : null}
      </div>
      <div className="flex gap-1">
        <button>
          <SearchIcon />
        </button>
        <button>
          <EventIcon />
        </button>
      </div>
    </nav>
  );
}

function monthString(date) {
  return `${month()[date.getMonth()]} ${date.getFullYear()}`;
}
