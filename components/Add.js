import { useForm } from "@utils/useForm";
import {
  dateInputFormat,
  dateInputTimeFormat,
  inputToDateFormat,
} from "@utils/date";
import { UserContext } from "@lib/context";
import { useContext, useEffect } from "react";
import { firestore } from "@lib/firebase";
import toast from "react-hot-toast";
import CloseIcon from "@mui/icons-material/Close";

export default function Add({ date = new Date(), setAddMenu }) {
  const { user } = useContext(UserContext);
  const [form, formChange, setForm] = useForm({
    date: new Date(date),
    date_Input: dateInputFormat(date),
    date_Input_Time: dateInputTimeFormat(date),
    endDate: new Date(date),
    endDate_Input: dateInputFormat(date),
    endDate_Input_Time: dateInputTimeFormat(date),
    detail: "",
    title: "",
  });

  const handleDateChange = (e) => {
    const target = e.target.name;
    const targetDate = inputToDateFormat(e.target.value);
    setForm({
      ...form,
      [target.split("_")[0]]: targetDate,
      [`${target.split("_")[0]}_Input`]: e.target.value,
    });
  };

  const handleTimeChange = (e) => {
    const target = e.target.name;
    const targetDate = new Date(form[`${target}`]);
    const [hours, minutes] = e.target.value.split(":");
    targetDate.setHours(hours, minutes);
    setForm({
      ...form,
      [target.split("_")[0]]: new Date(targetDate),
      [`${target.split("_")[0]}_Input_Time`]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    console.log(form);
    const firebaseDatesModel = {
      date: new Date(form.date),
      endDate: new Date(form.endDate),
      detail: form.detail,
      title: form.title,
      createBy: {
        displayName: user.displayName,
        photoURL: user.photoURL,
        uid: user.uid,
      },
      uid: user.uid,
    };
    console.log(firebaseDatesModel);
    firestore
      .collection("dates")
      .doc()
      .set(firebaseDatesModel)
      .then(() =>
        toast.success(`Successfuly added ${form.title} to calendar 🔥🔥🔥`)
      )
      .catch((err) => console.log(err.message));
  };
  return (
    <div className="add__container">
      <button onClick={() => setAddMenu(false)} className="cancle">
        <CloseIcon />
      </button>
      <form onSubmit={onSubmit}>
        <h4>title</h4>
        <input
          type="text"
          name="title"
          value={form.title}
          onChange={formChange}
        />
        <h4>detail</h4>
        <input
          type="text"
          name="detail"
          value={form.detail}
          onChange={formChange}
        />
        <h4>
          date
          <input
            type="date"
            value={form.date_Input}
            name="date_Input"
            onChange={handleDateChange}
          />
          <input
            type="time"
            value={form.date_Input_Time}
            name="date_Input"
            onChange={handleTimeChange}
          />
        </h4>
        <h4>
          endDate
          <input
            type="date"
            value={form.endDate_Input}
            name="endDate_Input"
            onChange={handleDateChange}
          />
          <input
            type="time"
            value={form.endDate_Input_Time}
            name="endDate_Input"
            onChange={handleTimeChange}
          />
        </h4>

        {/* <div>{JSON.stringify(form)}</div> */}
        <button type="submit">Add</button>
      </form>
    </div>
  );
}
