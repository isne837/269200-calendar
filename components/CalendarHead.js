import { days } from "@utils/date";

export default function CalendarHead() {
  return days().map((day) => <div key={day}>{day}</div>);
}
