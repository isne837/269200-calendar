import { addDays, setTime, addMonths, dateDiff } from "@utils/date";
import { firestore } from "@lib/firebase";
import { useState, useEffect } from "react";

export const useDayCalendar = (date, user) => {
  const [loading, setLoading] = useState(true);
  const [currentDate, setCurrentDate] = useState({
    date: date,
    appointment: [],
  });
  useEffect(() => {
    // async function getDataFromFirebase() {
    //   const cursor = new Date(date);
    //   addDays(cursor, 1);
    //   const ref = firestore
    //     .collection("dates")
    //     .where("date", ">", date)
    //     .where("date", "<", cursor)
    //     .orderBy("date", "asc");

    //   const appointment = (await ref.get()).docs.map((doc) => {
    //     return { id: doc.id, ...doc.data() };
    //   });
    //   setCurrentDate({
    //     date: new Date(date),
    //     appointment: appointment,
    //   });
    //   setLoading(false);
    // }
    const cursor = new Date(date);
    addDays(cursor, 1);
    const unsubscribe = firestore
      .collection("dates")
      .where("uid", "==", user.uid)
      .where("date", ">=", date)
      .where("date", "<", cursor)
      .orderBy("date", "asc")
      .onSnapshot((collection) => {
        const appointment = collection.docs.map((doc) => {
          return { id: doc.id, ...doc.data() };
        });
        setCurrentDate({
          ...currentDate,
          appointment: appointment,
        });
        setLoading(false);
      });

    // setLoading(true);
    // getDataFromFirebase();
    return () => unsubscribe();
  }, [date]);

  return [currentDate, loading];
};

export const useMonthCalendar = (date, user) => {
  const [loading, setLoading] = useState(true);
  const [lastMonth, setLastMonth] = useState([]);
  const [currentMonth, setCurrentMonth] = useState([]);
  const [nextMonth, setNextMonth] = useState([]);
  useEffect(() => {
    async function getDataFromFirebase() {
      const firstDate = fristDateOfMonth(date);
      let lastMonthArray = dateOfLastMonth(firstDate);
      let currentMonthArray = dateOfCurrentMonth(firstDate);
      let nextMonthArray = dateOfNextMonth(firstDate);

      const firstInList = currentMonthArray[0].date;
      const lastInList = nextMonthArray[0].date;

      const ref = firestore
        .collection("dates")
        .orderBy("date", "asc")
        .where("uid", "==", user.uid)
        .where("date", ">=", firstInList)
        .where("date", "<", lastInList);

      (await ref.get()).docs.forEach((doc) => {
        const template = { id: doc.id, ...doc.data() };
        const startDate = template.date.toDate();
        const endDate = template.endDate.toDate();
        const diffDays = dateDiff(startDate, endDate);
        const endLoop =
          startDate.getDate() - 1 + diffDays > currentMonthArray.length - 1
            ? currentMonthArray.length - 1
            : startDate.getDate() - 1 + diffDays;
        // console.log(startDate.getDate() - 1, diffDays, endLoop);
        for (let i = startDate.getDate() - 1; i <= endLoop; i++) {
          const appointment = [...currentMonthArray[i].appointment];
          appointment.push(template);
          currentMonthArray[i] = {
            ...currentMonthArray[i],
            appointment: appointment,
          };
        }
      });

      setLastMonth(lastMonthArray);
      setCurrentMonth(currentMonthArray);
      setNextMonth(nextMonthArray);
      setLoading(false);
    }

    getDataFromFirebase();
  }, [date, user.uid]);
  return [lastMonth, currentMonth, nextMonth, loading];
};

export const useWeekCalendar = (date, user) => {
  const [loading, setLoading] = useState(true);
  const [currentWeek, setCurrentWeek] = useState([]);
  useEffect(() => {
    const getDataFromFirebase = async () => {
      const firstDate = firstDateOfWeek(date);
      let currentWeek = dateOfCurrentWeek(firstDate);
      if (currentWeek.length > 0) {
        const firstInList = currentWeek[0].date;
        const lastInList = new Date(currentWeek[currentWeek.length - 1].date);
        addDays(lastInList, 1);

        const ref = firestore
          .collection("dates")
          .orderBy("date", "asc")
          .where("uid", "==", user.uid)
          .where("date", ">=", firstInList)
          .where("date", "<", lastInList);
        (await ref.get()).docs.forEach((doc) => {
          const template = { id: doc.id, ...doc.data() };
          const startDate = template.date.toDate();
          const endDate = template.endDate.toDate();
          const diffDays = dateDiff(startDate, endDate);
          const endloop =
            startDate.getDay() + diffDays > 6
              ? 6
              : startDate.getDay() + diffDays;
          for (let i = startDate.getDay(); i <= endloop; i++) {
            const appointment = [...currentWeek[i].appointment];
            appointment.push(template);
            currentWeek[i] = { ...currentWeek[i], appointment: appointment };
          }
        });
        setCurrentWeek(currentWeek);
      }
      setLoading(false);
    };
    getDataFromFirebase();
  }, [date]);

  return [currentWeek, loading];
};

function dateOfCurrentWeek(date) {
  const cursor = new Date(date);
  const currentWeek = [];
  for (let i = 0; i < 7; i++) {
    currentWeek.push({ date: new Date(cursor), appointment: [] });
    addDays(cursor, 1);
  }
  return currentWeek;
}

function firstDateOfWeek(date) {
  const firstDate = new Date(date);
  setTime(firstDate);
  while (firstDate.getDay() !== 0) addDays(firstDate, -1);
  return firstDate;
}

function fristDateOfMonth(date) {
  const firstDate = new Date(date);
  firstDate.setDate(1);
  setTime(firstDate);
  return firstDate;
}

function dateOfLastMonth(date) {
  const cursor = new Date(date);
  addDays(cursor, -(date.getDay() + 1));
  const lastMonth = [];
  for (let i = 0; i < date.getDay(); i++) {
    addDays(cursor, 1);
    lastMonth.push({ date: new Date(cursor), appointment: [] });
  }
  return lastMonth;
}

function dateOfCurrentMonth(date) {
  const cursor = new Date(date);
  const currentMonth = [];
  do {
    currentMonth.push({ date: new Date(cursor), appointment: [] });
    addDays(cursor, 1);
  } while (cursor.getMonth() === date.getMonth());
  return currentMonth;
}

function dateOfNextMonth(date) {
  const cursor = new Date(date);
  const saturdays = getSaturdays(date);
  addMonths(cursor, 1);
  addDays(cursor, -1);
  const nextMonth = [];
  let nextMonthDay = 14;
  saturdays === 5 ? (nextMonthDay = 7) : (nextMonthDay = 14);
  for (let i = cursor.getDay(); i < nextMonthDay - 1; i++) {
    addDays(cursor, 1);
    nextMonth.push({ date: new Date(cursor), appointment: [] });
  }
  return nextMonth;
}

function getSaturdays(date) {
  const cursor = new Date(date);
  while (cursor.getDay() !== 6) cursor.setDate(cursor.getDate() + 1);
  let saturdays = 0;
  while (cursor.getMonth() === date.getMonth()) {
    saturdays++;
    addDays(cursor, 7);
  }
  return saturdays;
}
