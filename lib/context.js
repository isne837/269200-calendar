import { createContext } from "react";

export const DateContext = createContext({
  cursor: new Date(),
  setCursor: null,
});

export const UserContext = createContext({
  user: null,
});
