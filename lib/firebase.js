import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyBQciac0ImxfM1ZUHkPgNBzLl3jS3Bc2_0",
  authDomain: "calendar-5235c.firebaseapp.com",
  projectId: "calendar-5235c",
  storageBucket: "calendar-5235c.appspot.com",
  messagingSenderId: "477036931511",
  appId: "1:477036931511:web:be48895ce2bb690b056032",
};

if (!firebase.apps.length > 0) firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();
export const facebookProvider = new firebase.auth.FacebookAuthProvider();
export const emailProvider = firebase.auth.EmailAuthProvider();

export const storage = firebase.storage();
export const STATE_CHANGED = firebase.storage.TaskEvent.STATE_CHANGED;

export const firestore = firebase.firestore();
export const Increment = firebase.firestore.FieldValue.increment;
export const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp;
export const arrayUnion = firebase.firestore.FieldValue.arrayUnion;
