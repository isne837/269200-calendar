import { useState } from "react";

export function useForm(init) {
  const [values, setValues] = useState(init);
  return [
    values,
    (e) =>
      setValues((old) => {
        return {
          ...old,
          [e.target.name]: e.target.value,
        };
      }),
    setValues,
  ];
}
