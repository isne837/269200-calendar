import { useState, useEffect } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth, firestore } from "@lib/firebase";

export function useUser() {
  const [loginAs, authLoading] = useAuthState(auth);
  const [user, setUser] = useState();
  const [loading, setLoading] = useState(authLoading);
  useEffect(() => {
    const getUser = async () => {
      if (loginAs) {
        setLoading(true);
        const ref = firestore.collection("usernames").doc(loginAs.uid);
        const data = await ref.get();

        if (!data.exists) {
          const creadentailsObject = {
            displayName: loginAs.displayName,
            photoURL: loginAs.photoURL,
            uid: loginAs.uid,
          };
          ref.set(creadentailsObject);
          setUser(creadentailsObject);
        } else setUser(data.data());
        setLoading(false);
      } else setUser(null);
    };

    getUser();
  }, [loginAs]);

  useEffect(() => {
    setLoading(authLoading);
  }, [authLoading]);

  return [user, loading];
}
