export const addDays = (date, day) => {
  return date.setDate(date.getDate() + day);
};

export const addMonths = (date, month) => {
  date.setMonth(date.getMonth() + month);
};

export const setTime = (date) => {
  return date.setHours(0, 0, 0, 0);
};

export const moveCursorDate = (date, day) => {
  const cursor = new Date(date);
  addDays(cursor, day);
  return new Date(cursor);
};

export const moveCursorMonth = (date, month) => {
  const cursor = new Date(date);
  addMonths(cursor, month);
  return new Date(cursor);
};

export const dateDiff = (date1, date2) => {
  const diffTime = Math.abs(date2 - date1);
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
  return diffDays;
};

export const month = () => [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export const days = () => ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

export const dateInputFormat = (date) => {
  if (typeof date === "object") {
    const [month, day, year] = date.toLocaleString().split(",")[0].split("/");
    //inputformat yyyy-mm-dd
    return `${year}-${month}-${day}`;
  } else return;
};

export const inputToDateFormat = (inputTypeDate) => {
  const [year, month, date] = inputTypeDate.split("-");
  return new Date(parseInt(year), parseInt(month) - 1, parseInt(date));
};

export const dateInputTimeFormat = (date) => {
  if (typeof date === "object") {
    const [hours, minites] = date.toString().split(" ")[4].split(":");
    return `${hours}:${minites}`;
  } else return;
};

export const stringDateFromFirebase = (date) => {
  const [d, month, day, year] = date.toDate().toString().split(" ");
  return `${d} ${month} ${day} ${year}`;
};

export const stringDateFromDate = (date) => {
  const [d, month, day, year] = date.toString().split(" ");
  return `${d} ${month} ${day} ${year}`;
};
